<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
	   return view('views.page.index');
    }
    //
    
    function userPage($id)
    {
        return view('test.user.datatable');
    }
    
    function userInfo()
    {
        $data = DB::table('open_leads')
            ->join('leads', 'open_leads.lead_id', '=', 'leads.id')
            ->join('customers', 'leads.customer_id', '=', 'customers.id')
            ->where('open_leads.agent_id', '=', Sentinel::getUser()->id)
            ->select('leads.id', 'leads.date', 'leads.name', 'customers.phone', 'leads.email')
            ->get();

        $data = ['headers' => ['id', 'date', 'name', 'phone', 'email'], 'data' => $data];
        $data = json_encode($data);
        
        return $data;
    }
    
    function userDetailInfo(Request $request)
    {
        $leadId = Request::ajax() ? Input::get('leadId') : null;
        
        $data = DB::table('sphere_attribute_options')
            ->join('sphere_attributes', 'sphere_attribute_options.sphere_attr_id', '=', 'sphere_attributes.id')
            ->join('leads', 'sphere_attributes.sphere_id', '=', 'leads.sphere_id')
            ->where('sphere_attribute_options.ctype', '=', 'agent')
            ->select('sphere_attribute_options.value')
            ->get();
        
        $data = ['headers' => ['id', 'date', 'name', 'phone', 'email', 'value'], 'data' => $data];
        $data = json_encode($data);
        
        return $data;
    }
}
