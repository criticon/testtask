@extends('layouts.master')

@section('content')
        <!-- Page Content -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User Data</h1>
                        <div>
                            <table id="leads">
                            </table>
                        </div>
                        <div>
                            <table id="leadinfo">
                            </table>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->

    <style type="text/css">
        table {
          border-collapse: collapse;
        }
        
        table, th, td {
          border: 1px solid black;
        }
        
        table#leads tr td:first-child {
            display: none;
        }
        
        table#leadinfo tr:first-child {
            display: none;
        }

    </style>
            
    <script type="text/javascript">

        function createHTable (data) {
            var header = '';
            var body = '';
            
            for (var headerIndex in data.headers) {
                if (headerIndex == 1) {
                    header += '<td>icon</td>';
                }
                header += '<td>' + data.headers[headerIndex] + '</td>';
            }
            header = '<tr>' + header + '</tr>';

            for (var itemIndex in data.data) {
                body += '<tr>'
                for (var propIndex in data.data[itemIndex]) {
                    if (propIndex == 'date') {
                        body += '<td></td>';
                    }
                    body += '<td>' + data.data[itemIndex][propIndex] + '</td>';
                }
                body += '</tr>';
            }

            return header + body;
        }

        function createVTable (additionalData, mainData, leadId) {
            var tr, td, commonData;

            for (var i in mainData.data) {
                if (mainData.data[i].id == leadId) {
                    commonData = mainData.data[i];
                    break;
                }
            }

            tr = '';
            for (var i in additionalData.headers) {
                td = '';
                for (var j in additionalData.data) {
                	commonData['value'] = additionalData.data[j]['value'];
                    td += '<td>' + commonData[additionalData.headers[i]] + '</td>';
                }
                
                tr += '<tr>' + '<td>' + additionalData.headers[i] + '</td>' + td + '</tr>';
            }

            return tr;
        }

        var leadsData = null;
        
        $(document).ready(function() {
            $.ajax({
               dataType: 'json',
               url: '{!! route("uinfo") !!}',
               success: function (jsonData) {
            	   leadsData = jsonData;
                   $('#leads').html(createHTable(jsonData));
               }
            });


            $('#leads').on('click', 'tr', function() {
                var leadId = $(this).children('td:first-child').text();
                
                $.ajax({
                   dataType: 'json',
                   data: {"leadId": leadId},
                   url: '{!! route("udinfo") !!}',
                   success: function (jsonData) {
                   $('#leadinfo').html(createVTable(jsonData, leadsData, leadId));
                   }
                });
                
            });
        });
    </script>

@endsection